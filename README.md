Vaš zadatak je da napravite kopiju sajta čija se slika nalazi u okviru ovog repozitorijuma (sajtZaIzradu.jpg)

Svako od vas će raditi po dve sekcije. Dobićete inicijalizovanu index.html stranicu i style.css fajl.

U index.html fajlu komentarima je naznačeno ko od vas radi koju sekciju. U proseku svako dobija po dve. Ukupno ima 8 sekcija.

KOMPLETIRANO, ZAJEDNO SA BOOTSTRAP-OM 29.08.2019.

Novi zadatak - zadat: 29.08.2019. JS

Imate zadatak da napravite sopstvene hmtl stranice sa sopstvenim css i js fajlovima koje povezujete sa datom stranicom. Naziv stranice je vaseimeHTML.html

Svako radi na svojoj Git grani.

1. Dodavanje/uklanjanje HTML sadrzaja

	Osoba ima sledece atribute: ime, prezime, pol, godine, zvanje

	David - Dodavanje osoba na osnovu izbora iz select kontrole (zaposleni, nezaposleni)
	Stefan - Dodavanje osoba na osnovu radio button-a (zaposleni, nezaposleni)
	Jovan - Dodavanje osoba na osnovu checkbox-a (zaposleni, nezaposleni)

2. Stilizovanje html-a
	
	David - Na klik dugmeta da se slovca u tabeli povecaju
	Stefan - Iz select kontrole se bira velicina fonta u tabeli
	Jovan - Iz select kontrole se bira boja pozadine tabele

3. Sakrivanje i prikazivanje tabele na odabir radio dugmica - SVI

4. Dodati formu za:

	David - Dodavanje knjiga - atribuiti su: naslov, godina izdanja, isbn
	Stefan - Dodavanje automobila - atributi su: marka, godiste, polovan (radio button)
	Jovan - Dodavanje oglasa - naslov oglasa, text oglasa (textarea), potrebne vestinevestine (checkbox)